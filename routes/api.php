<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function() {

    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->middleware('guest');
    Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register'])->middleware('guest');

    Route::group(['middleware' =>'auth:api'], function() {

        Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);

        //routes for employees
        // Route::get('/summary', [App\Http\Controllers\EmployeesController::class, 'summary']);
        Route::get('/employees', [App\Http\Controllers\EmployeesController::class, 'index']);
        Route::get('/employees/create', [App\Http\Controllers\EmployeesController::class, 'create']);
        Route::post('/employees', [App\Http\Controllers\EmployeesController::class, 'store']);
        Route::get('/employees/{id}', [App\Http\Controllers\EmployeesController::class, 'show']);
        Route::post('/employees/{id}', [App\Http\Controllers\EmployeesController::class, 'update']);
        Route::delete('/employees/delete/{id}', [App\Http\Controllers\EmployeesController::class, 'destroy']);


    });
});




