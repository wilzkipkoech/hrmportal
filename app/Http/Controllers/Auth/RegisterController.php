<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
    	$request->validate([
    		'name' => 'required',
    		'email' => 'required|unique:users,email',
    		'password' => 'required|confirmed|min:6',
            'password_confirmation'=>'min:6'
    	]);

    	$data = $request->all();
    	$this->create($data);

    	return $this->login($data);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    protected function login($data)
    {
    	$request = Request::create('api/v1/login', 'POST', [
            'email' => $data['email'],
            'password' => $data['password']
        ]);

        $request->headers->set('Origin', '*');

        return app()->handle($request);
    }
}
